import { ClerkProvider } from "@clerk/clerk-react";
import { Suspense, lazy } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import Loader from "./components/loader";

const HomeLayout = lazy(() => import("./components/home-layout"));
const HomePage = lazy(() => import("./pages/home-page"));
const ListingPage = lazy(() => import("./pages/listing-page"));
const TripPage = lazy(() => import("./pages/trip-page"));
const ReservationPage = lazy(() => import("./pages/reservation-page"));
const PropertiesPage = lazy(() => import("./pages/properties-page"));
const FavoritesPage = lazy(() => import("./pages/favorites-page"));
const ErrorPage = lazy(() => import("./pages/error-page"));

const clerkPubKey = process.env.REACT_APP_CLERK_PUBLISHABLE_KEY;

function App() {
  const navigate = useNavigate();

  return (
    <ClerkProvider
      publishableKey={clerkPubKey}
      navigate={(to) => navigate(to)}
      appearance={{
        elements: {
          socialButtonsBlockButton:
            "relative disabled:opacity-70 disabled:cursor-not-allowed rounded-lg hover:bg-white hover:opacity-80 transition w-full bg-white border-black text-black text-md py-3 font-bold border-2",
          formButtonPrimary:
            "relative disabled:opacity-70 disabled:cursor-not-allowed rounded-lg hover:bg-rose-500 hover:opacity-80 transition w-full bg-rose-500 border-rose-500 text-white text-md py-3 font-semibold border-2",
          formFieldInput:
            "w-full p-2 pt-3 font-light bg-white border-2 rounded-md outline-none transition disabled:opacity-70 disabled:cursor-not-allowed",
          formFieldLabel:
            "text-md text-zinc-400 duration-150 transform -translate-y-3 origin-[0]",
        },
        layout: {
          socialButtonsPlacement: "bottom",
        },
      }}
    >
      <Suspense fallback={<Loader />}>
        <Routes>
          <Route path="/" element={<HomeLayout />}>
            <Route index element={<HomePage />} />
            <Route path="/listings/:listingId" element={<ListingPage />} />
            <Route path="/trips" element={<TripPage />} />
            <Route path="/reservations" element={<ReservationPage />} />
            <Route path="/properties" element={<PropertiesPage />} />
            <Route path="/favorites" element={<FavoritesPage />} />
            <Route path="*" element={<ErrorPage />} />
          </Route>
        </Routes>
      </Suspense>
    </ClerkProvider>
  );
}

export default App;
