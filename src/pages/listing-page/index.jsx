import { useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { differenceInDays, eachDayOfInterval } from "date-fns";
import { useNavigate, useParams } from "react-router-dom";
import { getListingById } from "../../stores/slice/listingSlice";

import EmptyState from "../../components/empty-state";
import Container from "../../components/container";
import ListingHead from "../../components/listing/listing-head";
import ListingInfo from "../../components/listing/liisting-info";
import { categories } from "../../components/categories";
import ListingReservation from "../../components/listing/listing-reservation";
import {
  createReservation,
  getReservations,
} from "../../stores/slice/reservationSlice";

const initialDateRange = {
  startDate: new Date(),
  endDate: new Date(),
  key: "selection",
};

const ListingPage = () => {
  const navigate = useNavigate();
  const { listingId } = useParams();
  const dispatch = useDispatch();

  const detailListing = useSelector((state) => state.listing.detailListing);

  const reservations = useSelector((state) => state.reservation.reservations);
  const isLoading = useSelector((state) => state.reservation.isLoading);

  const [totalPrice, setTotalPrice] = useState(detailListing.price);
  const [dateRange, setDateRange] = useState(initialDateRange);

  const disabledDates = useMemo(() => {
    let dates = [];

    reservations.forEach((reservation) => {
      const range = eachDayOfInterval({
        start: new Date(reservation.startDate),
        end: new Date(reservation.endDate),
      });

      dates = [...dates, ...range];
    });

    return dates;
  }, [reservations]);

  const category = useMemo(() => {
    return categories.find((items) => items.label === detailListing.category);
  }, [detailListing.category]);

  const onCreateReservation = useCallback(() => {
    const data = {
      totalPrice,
      startDate: dateRange.startDate,
      endDate: dateRange.endDate,
      listingId: detailListing?._id,
      navigate,
    };
    dispatch(createReservation(data));
    setDateRange(initialDateRange);
  }, [totalPrice, dateRange, detailListing?._id, dispatch, navigate]);

  useEffect(() => {
    if (dateRange.startDate && dateRange.endDate) {
      const dayCount = differenceInDays(dateRange.endDate, dateRange.startDate);

      if (dayCount && detailListing.price) {
        setTotalPrice(dayCount * detailListing.price);
      } else {
        setTotalPrice(detailListing.price);
      }
    }
  }, [dateRange, detailListing.price]);

  useEffect(() => {
    dispatch(getListingById(listingId));
    dispatch(getReservations({ query: "listingId", id: listingId }));
  }, [listingId, dispatch]);

  if (!detailListing) {
    return <EmptyState />;
  }

  return (
    <Container>
      <div className="max-w-screen-lg mx-auto">
        <div className="flex flex-col gap-6">
          <ListingHead
            title={detailListing.title}
            imageSrc={detailListing.imageSrc}
            locationValue={detailListing.locationValue}
            id={detailListing._id}
          />
          <div className="grid grid-cols-1 mt-6 md:grid-cols-7 md:gap-10">
            <ListingInfo
              user={detailListing.userId}
              category={category}
              description={detailListing.description}
              roomCount={detailListing.roomCount}
              guestCount={detailListing.guestCount}
              bathroomCount={detailListing.bathroomCount}
              locationValue={detailListing.locationValue}
            />
            <div className="order-first mb-10 md:order-last md:col-span-3">
              <ListingReservation
                price={detailListing.price}
                totalPrice={totalPrice}
                onChangeDate={(value) => setDateRange(value)}
                dateRange={dateRange}
                onSubmit={onCreateReservation}
                disabled={isLoading}
                disabledDates={disabledDates}
              />
            </div>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default ListingPage;
