import { useDispatch, useSelector } from "react-redux";
import { deleteReservation, getReservations } from "../../stores/slice/reservationSlice";
import EmptyState from "../../components/empty-state";
import { useCallback, useEffect, useState } from "react";
import Container from "../../components/container";
import Heading from "../../components/heading";
import ListingCard from "../../components/listing/listing-card";
import { useUser } from "@clerk/clerk-react";
import Loader from "../../components/loader";

const TripPage = () => {
  const dispatch = useDispatch();
  const { isSignedIn } = useUser();

  const [deletingId, setDeletingId] = useState("");

  const currentUser = useSelector((state) => state.user.currentUser);
  const reservations = useSelector((state) => state.reservation.reservations);
  const isLoading = useSelector((state) => state.reservation.isLoading);

  const onCancel = useCallback((id) => {
    setDeletingId(id);

    dispatch(deleteReservation({ userId: currentUser._id, reservationId: id }));
    setDeletingId('');
  }, [dispatch, currentUser._id]);

  useEffect(() => {
    if (isSignedIn && currentUser._id) {
      dispatch(getReservations({ query: "userId", id: currentUser._id }));
    }
  }, [dispatch, currentUser._id, isSignedIn]);

  if (isLoading) {
    return <Loader />;
  }

  if (!isSignedIn) {
    return <EmptyState title="Unauthorized" subtitle="Please login" />;
  }

  if (reservations.length === 0) {
    return (
      <EmptyState
        title="No trips found"
        subtitle="Looks like you havent reserved any trips."
      />
    );
  }

  return (
    <Container>
      <Heading
        title="Trips"
        subtitle="Where you've been and where you're going"
      />
      <div className="grid grid-cols-1 gap-8 mt-10 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 2xl:grid-cols-6">
        {reservations.map((reservation) => (
          <ListingCard
            key={reservation._id}
            data={reservation.listingId}
            reservation={reservation}
            actionId={reservation._id}
            onAction={onCancel}
            disabled={deletingId === reservation._id}
            actionLabel="Cancel reservation"
          />
        ))}
      </div>
    </Container>
  );
};

export default TripPage;
