import React, { useEffect, useMemo } from "react";
import Container from "../../components/container";
import EmptyState from "../../components/empty-state";
import { useDispatch, useSelector } from "react-redux";
import { getListings } from "../../stores/slice/listingSlice";
import ListingCard from "../../components/listing/listing-card";
import { useSearchParams } from "react-router-dom";
import Loader from "../../components/loader";

const HomePage = () => {
  const dispatch = useDispatch();
  // eslint-disable-next-line no-unused-vars
  const [searchParams, setSearchParams] = useSearchParams();

  const params = useMemo(() => {
    const paramArr = [];
    for (let entry of searchParams.entries()) {
      paramArr.push(entry);
    }

    return paramArr;
  }, [searchParams]);

  const listings = useSelector((state) => state.listing.listings);
  const isLoading = useSelector((state) => state.listing.isLoading);

  useEffect(() => {
    dispatch(getListings(params));
  }, [params, dispatch]);

  if (isLoading) {
    return <Loader />;
  }

  if (listings.length === 0) {
    return <EmptyState showReset={true} />;
  }

  return (
    <Container>
      <div className="grid grid-cols-1 gap-8 pt-24 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 2xl:grid-cols-6">
        {listings.map((listing) => (
          <ListingCard key={listing._id} data={listing} />
        ))}
      </div>
    </Container>
  );
};

export default HomePage;
