import { useDispatch, useSelector } from "react-redux";
import EmptyState from "../../components/empty-state";
import { useUser } from "@clerk/clerk-react";
import { useCallback, useEffect, useState } from "react";
import { deleteListing, getListingByUserId } from "../../stores/slice/listingSlice";
import Container from "../../components/container";
import Heading from "../../components/heading";
import ListingCard from "../../components/listing/listing-card";
import Loader from "../../components/loader";

const PropertiesPage = () => {
  const dispatch = useDispatch();
  const { isSignedIn } = useUser();
  const currentUser = useSelector((state) => state.user.currentUser);
  const listings = useSelector((state) => state.listing.listings);
  const isLoading = useSelector((state) => state.listing.isLoading);

  const [deletingId, setDeletingId] = useState('');

  const onDelete = useCallback((id) => {
    setDeletingId(id);

    dispatch(deleteListing({userId: currentUser._id, listingId: id}))

    setDeletingId('');
  }, [currentUser._id, dispatch]);

  useEffect(() => {
    if (currentUser._id) {
      dispatch(getListingByUserId(currentUser._id));
    }
  }, [currentUser, dispatch]);

  if (isLoading) {
    return <Loader />;
  }

  if (!isSignedIn) {
    return <EmptyState title="Unauthorized" subtitle="Please login" />;
  }

  if (listings.length === 0) {
    return (
      <EmptyState
        title="No properties found"
        subtitle="Looks like you have no properties."
      />
    );
  }

  return (
    <Container>
      <Heading title="Properties" subtitle="List of your properties" />
      <div className="grid grid-cols-1 gap-8 mt-10 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 2xl:grid-cols-6">
        {listings.map((listing) => (
          <ListingCard
            key={listing._id}
            data={listing}
            actionId={listing._id}
            onAction={onDelete}
            disabled={deletingId === listing._id}
            actionLabel="Delete property"
            currentUser={currentUser}
          />
        ))}
      </div>
    </Container>
  );
};

export default PropertiesPage;
