import { useDispatch, useSelector } from "react-redux";
import EmptyState from "../../components/empty-state";
import Heading from "../../components/heading";
import Container from "../../components/container";
import ListingCard from "../../components/listing/listing-card";
import { useEffect } from "react";
import { getFavoriteListings } from "../../stores/slice/listingSlice";
import Loader from "../../components/loader";

const FavoritesPage = () => {
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.user.currentUser);
  const listings = useSelector((state) => state.listing.listings);
  const isLoading = useSelector((state) => state.listing.isLoading);

  useEffect(() => {
    if (currentUser._id) {
      dispatch(getFavoriteListings(currentUser._id));
    }
  }, [dispatch, currentUser._id]);

  if (isLoading) {
    return <Loader />;
  }

  if (listings.length === 0) {
    return (
      <EmptyState
        title="No favorites found"
        subtitle="Looks like you have no favorite listings."
      />
    );
  }

  return (
    <Container>
      <Heading title="Favorites" subtitle="List of places you favorited!" />
      <div className="grid grid-cols-1 gap-8 mt-10 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 2xl:grid-cols-6">
        {listings.map((listing) => (
          <ListingCard
            currentUser={currentUser}
            key={listing.id}
            data={listing}
          />
        ))}
      </div>
    </Container>
  );
};

export default FavoritesPage;
