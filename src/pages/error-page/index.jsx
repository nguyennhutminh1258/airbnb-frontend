import EmptyState from '../../components/empty-state'

const ErrorPage = () => {
  return (
    <EmptyState
      title="Uh Oh"
      subtitle="Something went wrong!"
    />
  )
}

export default ErrorPage