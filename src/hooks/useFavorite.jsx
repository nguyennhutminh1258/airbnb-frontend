import { useCallback, useMemo } from "react";
import { useDispatch } from "react-redux";
import {
  addFavoriteListing,
  removeFavoriteListing,
} from "../stores/slice/userSlice";

const useFavorite = ({ listingId, currentUser }) => {
  const dispatch = useDispatch();
  const data = useMemo(() => {
    return { currentUser, listingId };
  }, [currentUser, listingId]);

  const hasFavorited = useMemo(() => {
    const list = currentUser?.favoriteIds || [];

    return list.includes(listingId);
  }, [currentUser, listingId]);

  const toggleFavorite = useCallback(
    (e) => {
      e.stopPropagation();

      if (hasFavorited) {
        dispatch(removeFavoriteListing(data));
      } else {
        dispatch(addFavoriteListing(data));
      }
    },
    [data, dispatch, hasFavorited]
  );

  return {
    hasFavorited,
    toggleFavorite,
  };
};

export default useFavorite;
