import axios from "axios";
const baseURL = process.env.REACT_APP_BACK_END_API;

const api = axios.create({
  baseURL,
  withCredentials: true,
});

export default api;
