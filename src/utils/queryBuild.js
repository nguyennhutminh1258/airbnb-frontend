export const queryBuild = (queryArr) => {
    let queryStr = "";
    queryArr.map((item, index) =>
      index === 0
        ? (queryStr += `?${item[0]}=${item[1]}`)
        : (queryStr += `&${item[0]}=${item[1]}`)
    );

    return queryStr;
}