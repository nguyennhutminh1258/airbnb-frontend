import { combineReducers, configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { watcherSaga } from "./saga/rootSaga";

import userSlice from "./slice/userSlice";
import rentModalSlice from "./slice/rentModalSlice";
import listingSlice from "./slice/listingSlice";
import reservationSlice from "./slice/reservationSlice";
import searchModalSlice from "./slice/searchModalSlice";

const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware];

const reducer = combineReducers({
  user: userSlice,
  listing: listingSlice,
  reservation: reservationSlice,
  rentModal: rentModalSlice,
  searchModal: searchModalSlice,
});

const store = configureStore({ reducer, middleware });

sagaMiddleware.run(watcherSaga);

export default store;
