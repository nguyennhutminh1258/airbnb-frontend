import { createSlice } from "@reduxjs/toolkit";

const rentModalSlice = createSlice({
  name: "rentModal",
  initialState: {
    isOpen: false,
  },
  reducers: {
    setOpenRentModal(state, action) {
      state.isOpen = action.payload;
    },
  },
});

export const { setOpenRentModal } = rentModalSlice.actions;

export default rentModalSlice.reducer;
