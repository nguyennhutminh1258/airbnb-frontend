import { createSlice } from "@reduxjs/toolkit";

const reservationSlice = createSlice({
  name: "reservation",
  initialState: {
    reservations: [],
    isLoading: false,
    isMounted: false,
  },
  reducers: {
    createReservation(state, action) {},

    getReservations(state, action) {},

    deleteReservation(state, action) {},

    setReservation(state, action) {
      if (Array.isArray(action.payload)) {
        state.reservations = action.payload;
      } else {
        state.reservations.push(action.payload);
      }
    },

    setIsLoadingReservation(state, action) {
      state.isLoading = action.payload;
    },
  },
});

export const {
  createReservation,
  getReservations,
  deleteReservation,
  setReservation,
  setIsLoadingReservation,
} = reservationSlice.actions;

export default reservationSlice.reducer;
