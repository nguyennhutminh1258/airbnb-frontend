import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
  name: "user",
  initialState: {
    currentUser: {},
  },
  reducers: {
    initialUser(state, action) {},

    addFavoriteListing(state, action) {},

    removeFavoriteListing(state, action) {},

    setCurrentUser(state, action) {
      const userData = action.payload;
      return { ...state, currentUser: userData };
    },
  },
});

export const {
  initialUser,
  addFavoriteListing,
  removeFavoriteListing,
  setCurrentUser,
} = userSlice.actions;

export default userSlice.reducer;
