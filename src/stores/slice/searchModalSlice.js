import { createSlice } from "@reduxjs/toolkit";

const searchModalSlice = createSlice({
  name: "searchModal",
  initialState: {
    isOpen: false,
  },
  reducers: {
    setOpenSearchModal(state, action) {
      state.isOpen = action.payload;
    },
  },
});

export const { setOpenSearchModal } = searchModalSlice.actions;

export default searchModalSlice.reducer;
