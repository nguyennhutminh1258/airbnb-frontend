import { createSlice } from "@reduxjs/toolkit";

const listingSlice = createSlice({
  name: "listing",
  initialState: {
    listings: [],
    isLoading: false,
    detailListing: {}
  },
  reducers: {
    createListing(state, action) {},

    getListings(state, action) {},

    getListingById(state, action) {},

    getListingByUserId(state, action) {},

    getFavoriteListings(state, action) {},

    deleteListing(state, action) {},

    setListing(state, action) {
      if (Array.isArray(action.payload)) {
        state.listings = action.payload;
      }
      else {
        state.listings.push(action.payload);
      }
    },

    setDetaiListing(state, action) {
      state.detailListing = action.payload;
    },

    setIsLoadingListing(state, action) {
      state.isLoading = action.payload;
    },
  },
});

export const {
  createListing,
  getListings,
  getListingById,
  getListingByUserId,
  getFavoriteListings,
  deleteListing,
  setListing,
  setDetaiListing,
  setIsLoadingListing,
} = listingSlice.actions;

export default listingSlice.reducer;
