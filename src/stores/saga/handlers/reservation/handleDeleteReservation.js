import toast from "react-hot-toast";
import { requestDeleteReservation } from "../../requests/reservation/requestDeleteReservation";
import { call } from "redux-saga/effects";

export function* handleDeleteReservation(action) {
  try {
    const res = yield call(requestDeleteReservation, action.payload);
    yield toast.success(res.data.msg);
    yield window.location.reload();
  } catch (error) {
    toast.error("Something went wrong.");
  }
}
