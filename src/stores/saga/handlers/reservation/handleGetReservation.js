import { call, put } from "redux-saga/effects";
import { setIsLoadingReservation, setReservation } from "../../../slice/reservationSlice";
import { requestGetReservation } from "../../requests/reservation/requestGetReservation";
import toast from "react-hot-toast";

export function* handleGetReservation(action) {
  yield put(setIsLoadingReservation(true));
  try {
    const res = yield call(requestGetReservation, action.payload);
    yield put(setReservation(res.data.reservation));
  } catch (error) {
    toast.error("Something went wrong.");
  }
  yield put(setIsLoadingReservation(false));
}
