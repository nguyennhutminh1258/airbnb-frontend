import { call, put, select } from "redux-saga/effects";
import toast from "react-hot-toast";
import {
  setIsLoadingReservation,
  setReservation,
} from "../../../slice/reservationSlice";
import { requestCreateReservation } from "../../requests/reservation/requestCreateReservation";

export function* handleCreateReservation(action) {
  yield put(setIsLoadingReservation(true));
  try {
    const { totalPrice, startDate, endDate, listingId, navigate } =
      action.payload;
    const currentUser = yield select((state) => state.user.currentUser);

    const data = {
      totalPrice,
      startDate,
      endDate,
      listingId,
      userId: currentUser._id,
    };
    const res = yield call(requestCreateReservation, data);
    if (res.data.reservation) {
      yield toast.success(res.data.msg);
      yield put(setReservation(res.data.reservation));
      yield navigate("/trips");
    }
  } catch (error) {
    toast.error("Something went wrong.");
  } finally {
    yield put(setIsLoadingReservation(false));
  }
}
