import { call, put } from "redux-saga/effects";
import { setIsLoadingListing, setListing } from "../../../slice/listingSlice";
import { requestGetListingByUserId } from "../../requests/listing/requestGetListingByUserId";
import toast from "react-hot-toast";

export function* handleGetListingByUserId(action) {
  yield put(setIsLoadingListing(true));
  try {
    const res = yield call(requestGetListingByUserId, action.payload);
    yield put(setListing(res.data.listing));
  } catch (error) {
    toast.error("Something went wrong.");
  }
  yield put(setIsLoadingListing(false));
}
