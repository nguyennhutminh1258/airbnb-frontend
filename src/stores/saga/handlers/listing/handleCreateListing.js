import { call, put, select } from "redux-saga/effects";
import { requestCreateListing } from "../../requests/listing/requestCreateListing";
import { setIsLoadingListing, setListing } from "../../../slice/listingSlice";
import toast from "react-hot-toast";
import { setOpenRentModal } from "../../../slice/rentModalSlice";

export function* handleCreateListing(action) {
  yield put(setIsLoadingListing(true));
  try {
    const currentUser = yield select((state) => state.user.currentUser);

    const data = { ...action.payload, userId: currentUser._id };
    const res = yield call(requestCreateListing, data);
    if (res.data.listing) {
      yield toast.success(res.data.msg);
      yield put(setOpenRentModal(false));
      yield put(setListing(res.data.listing));
    }
  } catch (error) {
    toast.error("Something went wrong.");
  } finally {
    yield put(setIsLoadingListing(false));
  }
  // yield window.location.reload();
}
