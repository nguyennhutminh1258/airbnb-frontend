import { call, put } from "redux-saga/effects";
import { setIsLoadingListing, setListing } from "../../../slice/listingSlice";
import toast from "react-hot-toast";
import { requestGetFavoriteListings } from "../../requests/listing/requestGetFavoriteListings";

export function* handleGetFavoriteListings(action) {
  yield put(setIsLoadingListing(true));
  try {
    const res = yield call(requestGetFavoriteListings, action.payload);
    yield put(setListing(res.data.listing));
  } catch (error) {
    toast.error("Something went wrong.");
  }
  yield put(setIsLoadingListing(false));
}