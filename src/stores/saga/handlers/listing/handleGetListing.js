import { call, put } from "redux-saga/effects";
import { setIsLoadingListing, setListing } from "../../../slice/listingSlice";
import toast from "react-hot-toast";
import { requestGetListings } from "../../requests/listing/requestGetListings";
import { queryBuild } from "../../../../utils/queryBuild";

export function* handleGetListings(action) {
  yield put(setIsLoadingListing(true));
  try {
    let queryStr = queryBuild(action.payload);
    const res = yield call(requestGetListings, queryStr);
    yield put(setListing(res.data.listing));
  } catch (error) {
    toast.error("Something went wrong.");
  }
  yield put(setIsLoadingListing(false));
}
