import { call } from "redux-saga/effects";
import { requestDeleteListing } from "../../requests/listing/requestDeleteListing";
import toast from "react-hot-toast";

export function* handleDeleteListing(action) {
    try {
        const res = yield call(requestDeleteListing, action.payload);
        yield toast.success(res.data.msg);
        yield window.location.reload();
    } catch (error) {
        toast.error('Something went wrong.')
    }
}