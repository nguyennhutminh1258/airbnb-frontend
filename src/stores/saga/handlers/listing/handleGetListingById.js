import { call, put } from "redux-saga/effects";
import { setDetaiListing, setIsLoadingListing } from "../../../slice/listingSlice";
import toast from "react-hot-toast";
import { requestGetListingById } from "../../requests/listing/requestGetListingById";

export function* handleGetListingById(action) {
  yield put(setIsLoadingListing(true));
  try {
    const res = yield call(requestGetListingById, action.payload);
    yield put(setDetaiListing(res.data.listing));
  } catch (error) {
    toast.error("Something went wrong.");
  }
  yield put(setIsLoadingListing(false));
}
