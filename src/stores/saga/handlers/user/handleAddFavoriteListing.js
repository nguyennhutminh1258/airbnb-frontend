import { call, put } from "redux-saga/effects";
import { setCurrentUser } from "../../../slice/userSlice";
import { requestAddFavoriteListing } from "../../requests/user/requestAddFavoriteListing";
import toast from "react-hot-toast";

export function* handleAddFavoriteListing(action) {
  try {
    const res = yield call(requestAddFavoriteListing, action.payload);
    
    yield toast.success(res.data.msg);
    yield put(setCurrentUser(res.data.user));
  } catch (error) {
    toast.error("Something went wrong." + error.response.data.msg);
  }
}
