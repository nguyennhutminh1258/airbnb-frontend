import { call, put } from "redux-saga/effects";
import { setCurrentUser } from "../../../slice/userSlice";
import { requestInitialUser } from "../../requests/user/requestInitialUser";

export function* handleInitialUser(action) {
  try {
    const res = yield call(requestInitialUser, action.payload);
    yield put(setCurrentUser(res.data.user));
  } catch (error) {
    console.log(error.response.data.msg)
  }
}
