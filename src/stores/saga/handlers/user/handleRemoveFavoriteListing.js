import { call, put } from "redux-saga/effects";
import { requestRemoveFavoriteListing } from "../../requests/user/requestRemoveFavoriteListing";
import { setCurrentUser } from "../../../slice/userSlice";
import toast from "react-hot-toast";

export function* handleRemoveFavoriteListing(action) {
  try {
    const res = yield call(requestRemoveFavoriteListing, action.payload);
    
    yield toast.success(res.data.msg);
    yield put(setCurrentUser(res.data.user));
  } catch (error) {
    toast.error("Something went wrong." + error.response.data.msg);
  }
}
