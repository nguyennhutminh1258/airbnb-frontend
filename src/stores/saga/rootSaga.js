import { takeLatest } from "redux-saga/effects";
import { addFavoriteListing, initialUser, removeFavoriteListing } from "../slice/userSlice";
import { handleInitialUser } from "./handlers/user/handleInitialUser";
import { createListing, deleteListing, getFavoriteListings, getListingById, getListingByUserId, getListings } from "../slice/listingSlice";
import { handleCreateListing } from "./handlers/listing/handleCreateListing";
import { handleGetListings } from "./handlers/listing/handleGetListing";
import { handleAddFavoriteListing } from "./handlers/user/handleAddFavoriteListing";
import { handleRemoveFavoriteListing } from "./handlers/user/handleRemoveFavoriteListing";
import { handleGetListingById } from "./handlers/listing/handleGetListingById";
import { createReservation, deleteReservation, getReservations } from "../slice/reservationSlice";
import { handleCreateReservation } from "./handlers/reservation/handleCreateReservation";
import { handleGetReservation } from "./handlers/reservation/handleGetReservation";
import { handleDeleteReservation } from "./handlers/reservation/handleDeleteReservation";
import { handleGetListingByUserId } from "./handlers/listing/handleGetListingByUserId";
import { handleDeleteListing } from "./handlers/listing/handleDeleteListing";
import { handleGetFavoriteListings } from "./handlers/listing/handleGetFavoriteListings";

export function* watcherSaga() {
  // User action
  yield takeLatest(initialUser.type, handleInitialUser);
  yield takeLatest(addFavoriteListing.type, handleAddFavoriteListing);
  yield takeLatest(removeFavoriteListing.type, handleRemoveFavoriteListing);

  // Listing action
  yield takeLatest(createListing.type, handleCreateListing);
  yield takeLatest(getListings.type, handleGetListings);
  yield takeLatest(getListingById.type, handleGetListingById);
  yield takeLatest(getListingByUserId.type, handleGetListingByUserId);
  yield takeLatest(getFavoriteListings.type, handleGetFavoriteListings);
  yield takeLatest(deleteListing.type, handleDeleteListing);

  // Reservation action
  yield takeLatest(createReservation.type, handleCreateReservation);
  yield takeLatest(getReservations.type, handleGetReservation);
  yield takeLatest(deleteReservation.type, handleDeleteReservation);
}
