import api from "../../../../config/axiosConfig";

export function requestGetReservation(payload) {
  return api.get(`api/reservation/?${payload.query}=${payload.id}`);
}
