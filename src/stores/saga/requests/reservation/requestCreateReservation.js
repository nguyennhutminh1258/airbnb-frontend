import api from "../../../../config/axiosConfig";

export function requestCreateReservation(payload) {  
  return api.post(`api/reservation`, payload);
}

