import api from "../../../../config/axiosConfig";

export function requestDeleteReservation(payload) {
  return api.delete(`api/reservation/${payload.userId}?reservationId=${payload.reservationId}`);
}
