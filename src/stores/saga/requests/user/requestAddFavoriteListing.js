import api from "../../../../config/axiosConfig";

export function requestAddFavoriteListing(payload) {  
  return api.post(`api/favorite/add`, payload);
}

