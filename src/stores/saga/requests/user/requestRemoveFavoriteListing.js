import api from "../../../../config/axiosConfig";

export function requestRemoveFavoriteListing(payload) {  
  return api.post(`api/favorite/remove`, payload);
}

