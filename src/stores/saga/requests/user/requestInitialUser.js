import api from "../../../../config/axiosConfig";

export function requestInitialUser(payload) {  
  return api.post(`api/user/initial-user`, payload);
}

