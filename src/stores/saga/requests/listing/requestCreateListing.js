import api from "../../../../config/axiosConfig";

export function requestCreateListing(payload) {  
  return api.post(`api/listing`, payload);
}

