import api from "../../../../config/axiosConfig";

export function requestGetListingById(payload) {  
  return api.get(`api/listing/${payload}`);
}

