import api from "../../../../config/axiosConfig";

export function requestGetFavoriteListings(payload) {  
  return api.get(`api/favorite/${payload}`);
}

