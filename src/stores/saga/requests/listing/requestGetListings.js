import api from "../../../../config/axiosConfig";

export function requestGetListings(payload) {  
  return api.get(`api/listing/${payload}`);
}

