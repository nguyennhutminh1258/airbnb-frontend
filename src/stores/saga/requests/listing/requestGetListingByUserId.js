import api from "../../../../config/axiosConfig";

export function requestGetListingByUserId(payload) {  
  return api.get(`api/listing/?userId=${payload}`);
}

