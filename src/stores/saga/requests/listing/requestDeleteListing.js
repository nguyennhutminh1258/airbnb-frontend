import api from "../../../../config/axiosConfig";

export function requestDeleteListing(payload) {
  return api.delete(`api/listing/${payload.userId}?listingId=${payload.listingId}`);
}
