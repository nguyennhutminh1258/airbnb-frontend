import { useNavigate } from "react-router-dom";

const Logo = () => {
    const navigate = useNavigate();
  
    return ( 
      <img
        onClick={() => navigate('/')}
        className="hidden md:block cursor-pointer" 
        src="/images/logo.png" 
        height="100" 
        width="100" 
        alt="Logo" 
      />
     );
  }
   
  export default Logo;