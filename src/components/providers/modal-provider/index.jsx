import RentModal from "../../modals/rent-modal";
import SearchModal from "../../modals/search-modal";

const ModalsProvider = () => {
  return ( 
    <>
      <SearchModal />
      <RentModal />
    </>
   );
}
 
export default ModalsProvider;