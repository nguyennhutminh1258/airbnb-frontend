import { PuffLoader } from "react-spinners";

const Loader = () => {
  return (
    <div
      className="
      absolute
      top-0
      w-[100vw]
      h-[100vh]
      flex 
      flex-col 
      justify-center 
      items-center 
    "
    >
      <PuffLoader size={100} color="red" />
    </div>
  );
};

export default Loader;
