import useCountries from "../../../hooks/useCountries";
import Heading from "../../heading";
import HeartButton from "../../heart-button";

const ListingHead = ({ title, locationValue, imageSrc, id }) => {
  const { getByValue } = useCountries();

  const location = getByValue(locationValue);
  return (
    <>
      <Heading
        title={title}
        subtitle={`${location?.region}, ${location?.label}`}
      />
      <div className="w-full h-[60vh] overflow-hidden rounded-xl relative">
        <img
          src={imageSrc}
          className="object-cover w-full"
          alt="description"
        />
        <div className="absolute top-5 right-5">
          <HeartButton listingId={id}/>
        </div>
      </div>
    </>
  );
};

export default ListingHead;
