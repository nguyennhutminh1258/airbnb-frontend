import { SignInButton, useUser } from "@clerk/clerk-react";
import Button from "../../button";
import Calendar from "../../inputs/calendar";

const ListingReservation = ({
  price,
  dateRange,
  totalPrice,
  onChangeDate,
  onSubmit,
  disabled,
  disabledDates,
}) => {
  const { isSignedIn } = useUser();

  return (
    <div className="bg-white rounded-xl border-[1px] border-neutral-200 overflow-hidden">
      <div className="flex flex-row items-center gap-1 p-4">
        <div className="text-2xl font-semibold">$ {price}</div>
        <div className="font-light text-neutral-600">/ night</div>
        <hr />
      </div>
      <div className="flex items-center justify-center w-full">

        <Calendar
          value={dateRange}
          disabledDates={disabledDates}
          onChange={(value) => onChangeDate(value.selection)}
        />
      </div>
      <hr />
      <div className="p-4">
        {isSignedIn ? (
          <Button disabled={disabled} label="Reserve" onClick={onSubmit} />
        ) : (
          <SignInButton mode="modal" afterSignInUrl="/">
            <Button disabled={disabled} label="Reserve" />
          </SignInButton>
        )}
      </div>
      <hr />
      <div className="flex flex-row items-center justify-between p-4 text-lg font-semibold ">
        <div>Total</div>
        <div>$ {totalPrice}</div>
      </div>
    </div>
  );
};

export default ListingReservation;
