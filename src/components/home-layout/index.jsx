import { Outlet } from "react-router-dom";
import Navbar from "../navbar";
import ToasterProvider from "../providers/toaster-provider";
import ModalsProvider from "../providers/modal-provider";

const HomeLayout = () => {
  return (
    <>
      <ToasterProvider />
      <ModalsProvider />
      <Navbar />
      <div className="pb-20 pt-28">
        <Outlet />
      </div>
    </>
  );
};

export default HomeLayout;
