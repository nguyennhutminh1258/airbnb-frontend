import { useDispatch, useSelector } from "react-redux";
import Avatar from "../avatar";
import { AiOutlineMenu } from "react-icons/ai";
import { useCallback, useState } from "react";
import MenuItem from "../menu-item";
import { useNavigate } from "react-router-dom";
import {
  SignInButton,
  SignOutButton,
  SignUpButton,
  useClerk,
} from "@clerk/clerk-react";
import { setOpenRentModal } from "../../stores/slice/rentModalSlice";

const UserMenu = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const currentUser = useSelector((state) => state.user.currentUser);
  const { signOut } = useClerk();

  const [isOpen, setIsOpen] = useState(false);

  const toggleOpen = useCallback(() => {
    setIsOpen((value) => !value);
  }, []);

  const onRent = useCallback(() => {
    dispatch(setOpenRentModal(true));
  }, [dispatch]);

  return (
    <div className="relative">
      <div className="flex flex-row items-center gap-3">
        {Object.keys(currentUser).length > 0 ? (
          <div
            onClick={onRent}
            className="hidden px-4 py-3 text-sm font-semibold transition rounded-full cursor-pointer md:block hover:bg-neutral-100">
            Airbnb your home
          </div>
        ) : (
          <SignInButton mode="modal" afterSignInUrl="/">
            <div className="hidden px-4 py-3 text-sm font-semibold transition rounded-full cursor-pointer md:block hover:bg-neutral-100">
              Airbnb your home
            </div>
          </SignInButton>
        )}
        <div
          onClick={toggleOpen}
          className="p-4 md:py-1 md:px-2 border-[1px] border-neutral-200 flex flex-row items-center gap-3 rounded-full cursor-pointer hover:shadow-md transition"
        >
          <AiOutlineMenu />
          <div className="hidden md:block">
            <Avatar src={currentUser?.imageUrl} />
          </div>
        </div>
      </div>
      {isOpen && (
        <div className="absolute rounded-xl shadow-md w-[40vw] md:w-3/4 bg-white overflow-hidden right-0 top-12 text-sm">
          <div className="flex flex-col cursor-pointer">
            {Object.keys(currentUser).length > 0 ? (
              <>
                <MenuItem label="My trips" onClick={() => navigate("/trips")} />
                <MenuItem
                  label="My favorites"
                  onClick={() => navigate("/favorites")}
                />
                <MenuItem
                  label="My reservations"
                  onClick={() => navigate("/reservations")}
                />
                <MenuItem
                  label="My properties"
                  onClick={() => navigate("/properties")}
                />
                <MenuItem label="Airbnb your home" onClick={onRent} />
                <hr />
                <SignOutButton
                  signOutCallback={() => {
                    signOut();
                    window.location.reload();
                  }}
                >
                  <MenuItem label="Logout" />
                </SignOutButton>
              </>
            ) : (
              <>
                <SignInButton mode="modal" afterSignInUrl="/">
                  <MenuItem label="Login" />
                </SignInButton>
                <SignUpButton mode="modal" afterSignUpUrl="/">
                  <MenuItem label="Sign up" />
                </SignUpButton>
              </>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default UserMenu;
