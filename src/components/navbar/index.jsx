import React, { useEffect } from "react";
import Container from "../container";
import { useUser } from "@clerk/clerk-react";
import { useDispatch } from "react-redux";
import { initialUser } from "../../stores/slice/userSlice";
import Logo from "../logo";
import Search from "../search";
import UserMenu from "../user-menu";
import Categories from "../categories";

const Navbar = () => {
  const dispatch = useDispatch();
  const { isSignedIn, user } = useUser();

  useEffect(() => {
    if (isSignedIn) {
      dispatch(initialUser(user));
    }
  }, [isSignedIn, user, dispatch]);
  return (
    <div className="fixed w-full bg-white z-10 shadow-sm">
      <div
        className="
          py-4 
          border-b-[1px]
        "
      >
        <Container>
          <div
            className="
            flex 
            flex-row 
            items-center 
            justify-between
            gap-3
            md:gap-0
          "
          >
            <Logo />
            <Search />
            <UserMenu />
          </div>
        </Container>
      </div>

      <Categories />
    </div>
  );
};

export default Navbar;
