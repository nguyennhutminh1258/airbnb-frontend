import { TbPhotoPlus } from "react-icons/tb";
import { IoMdClose } from "react-icons/io";
import { imageUpload } from "../../../utils/imageUpload";

const ImageUpload = ({ onChange, value }) => {
  const handleChooseImage = async (e) => {
    let imageUrls = [];

    imageUrls = await imageUpload(e.target.files);
    onChange(imageUrls[0].url);
  };

  const handleRemoveImage = () => {
    onChange("");
  }

  return (
    <div
      className="
        relative
        transition
        border-dashed 
        border-2 
        p-20 
        border-neutral-300
        flex
        flex-col
        justify-center
        items-center
        gap-4
        text-neutral-600
      "
    >
      <input
        hidden
        accept="image/*"
        type="file"
        id="file"
        onChange={(e) => handleChooseImage(e)}
      />
      <label
        htmlFor="file"
        className="cursor-pointer flex flex-col justify-center items-center gap-4 font-semibold text-lg"
      >
        <TbPhotoPlus size={50} />
        Click to upload
      </label>
      {value && (
        <div className="absolute inset-0 w-full h-full">
          <div className="absolute top-3 right-3 w-6 h-6 rounded-full bg-rose-500 text-white hover:opacity-80 flex justify-center items-center text-xl cursor-pointer" onClick={handleRemoveImage}>
            <IoMdClose />
          </div>
          <img className="w-full h-full object-cover" src={value} alt="House" />
        </div>
      )}
    </div>
  );
};

export default ImageUpload;
